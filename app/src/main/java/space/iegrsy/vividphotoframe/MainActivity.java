package space.iegrsy.vividphotoframe;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements PicturesFolderFragment.OnFragmentInteractionListener, BSImagePicker.OnMultiImageSelectedListener, BSImagePicker.ImageLoaderDelegate {
    private static final int PERMISSON_REQUEST_CODE = 11;

    @BindView(R.id.main_txt_clock)
    TextView txtClock;

    @BindView(R.id.main_container_folders)
    FrameLayout containerFolders;

    @BindView(R.id.main_container_picture)
    FrameLayout containerPicture;

    private Handler handlerTimer = new Handler();

    PictureSliderFragment sliderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Timber.plant(new Timber.DebugTree());
    }

    private void initUI() {
        // Set font
        Typeface typeface = ResourcesCompat.getFont(this, R.font.crass_roots);
        txtClock.setTypeface(typeface);
        txtClock.bringToFront();
        txtClock.setOnClickListener(v -> {
            BSImagePicker multiSelectionPicker = new BSImagePicker.Builder("space.iegrsy.vividphotoframe.fileprovider")
                    .isMultiSelect()
                    .setSpanCount(15)
                    .disableOverSelectionMessage()
                    .build();

            multiSelectionPicker.show(getSupportFragmentManager(), "Image picker");
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.main_container_folders, new PicturesFolderFragment()).commit();

        sliderFragment = new PictureSliderFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container_picture, sliderFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!CheckPermissions())
            return;

        initUI();

        handlerTimer.post(runnableTimeUpdater);
        Timber.d("Time updater start.");
    }

    @Override
    protected void onPause() {
        super.onPause();

        handlerTimer.removeCallbacks(runnableTimeUpdater);
        Timber.d("Time updater stop.");
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Exit app?");
        builder.setPositiveButton(android.R.string.ok, (d, w) -> System.exit(0));
        builder.setNegativeButton(android.R.string.no, null);
        builder.create().show();
    }

    private boolean CheckPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                        },
                        PERMISSON_REQUEST_CODE);
            } else {
                Toast.makeText(this, "Need all permissions", Toast.LENGTH_SHORT).show();
            }

            return false;
        }

        return true;
    }

    private final Runnable runnableTimeUpdater = new Runnable() {
        @Override
        public void run() {
            long ts = System.currentTimeMillis();
            txtClock.setText(String.valueOf(ts));
            String date = new SimpleDateFormat("HH:mm:ss\ndd/MM/yyyy", Locale.getDefault()).format(new java.util.Date(ts));
            txtClock.setText(date);

            handlerTimer.removeCallbacks(runnableTimeUpdater);
            handlerTimer.postDelayed(runnableTimeUpdater, 1000);
        }
    };

    @Override
    public void onFolderSelected(Uri uri) {
        Timber.d("Folder selected: %s", uri.toString());

        File images = new File(String.format("%s/%s", Environment.getExternalStorageDirectory().getPath(), uri.toString()));
        String[] fileList = images.list();

        ArrayList<String> arrayList = new ArrayList<>();

        for (String file : fileList)
            if (file.contains(".jpg") || file.contains(".jpeg") || file.contains(".png"))
                arrayList.add(String.format("%s/%s", images.getPath(), file));

        sliderFragment.setSliderPhotoArray(arrayList);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        Timber.d("Selected images: %s", uriList.toString());

        ArrayList<String> arrayList = new ArrayList<>();
        for (Uri file : uriList)
            arrayList.add(file.toString());

        sliderFragment.setSliderPhotoArray(arrayList);
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(MainActivity.this).load(imageFile).into(ivImage);
    }
}
