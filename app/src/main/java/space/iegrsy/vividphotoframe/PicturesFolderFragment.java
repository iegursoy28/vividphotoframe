package space.iegrsy.vividphotoframe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

public class PicturesFolderFragment extends Fragment {
    private ListView listView;

    private Context context;
    private OnFragmentInteractionListener mListener;

    public PicturesFolderFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pictures_folder, container, false);

        listView = view.findViewById(R.id.list_folders);
        listView.setAdapter(new FolderListAdapter(context));
        listView.setOnItemClickListener(onItemClickListener);

        return view;
    }

    private final AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = view.findViewById(R.id.folder_txt);
            if (textView != null)
                onFolderPressed(Uri.parse((String) textView.getTag()));
        }
    };

    public void onFolderPressed(Uri uri) {
        if (mListener != null)
            mListener.onFolderSelected(uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFolderSelected(Uri uri);
    }

    public static class FolderListAdapter extends BaseAdapter {
        private final Context context;

        private ArrayList<String> folderList = new ArrayList<>();

        public FolderListAdapter(Context context) {
            this.context = context;

            File extStorageDir = Environment.getExternalStorageDirectory();
            String[] fileList = extStorageDir.list();
            folderList.addAll(Arrays.asList(fileList));
        }

        @Override
        public int getCount() {
            return folderList.size();
        }

        @Override
        public String getItem(int position) {
            return folderList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            @SuppressLint("ViewHolder")
            View view = LayoutInflater.from(context).inflate(R.layout.folder_layout_item, null);

            TextView textView = view.findViewById(R.id.folder_txt);
            textView.setText(folderList.get(position));
            textView.setTag(folderList.get(position));

            return view;
        }
    }

    public static class ImageFileFilter implements FileFilter {
        private final String[] okFileExtensions = new String[]{"jpg", "png", "gif", "jpeg"};

        File file;

        public ImageFileFilter(File file) {
            this.file = file;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension))
                    return true;
            }
            return false;
        }
    }
}
