package space.iegrsy.vividphotoframe;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PictureSliderFragment extends Fragment {
    private SliderView sliderView;

    public PictureSliderFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picture_slider, container, false);

        sliderView = view.findViewById(R.id.imageSlider);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();

        return view;
    }

    public void setSliderPhotoArray(@NonNull ArrayList<String> list) {
        SliderAdapterExample adapter = new SliderAdapterExample(list);
        sliderView.setSliderAdapter(adapter);
    }

    public static class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {
        private ArrayList<String> dataList;

        public SliderAdapterExample(@NonNull ArrayList<String> list) {
            this.dataList = list;
        }

        @Override
        public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
            return new SliderAdapterVH(inflate);
        }

        @Override
        public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
            File file = new File(dataList.get(position));
            Date lastModDate = new Date(file.lastModified());
            String date = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.getDefault()).format(lastModDate);

            viewHolder.textViewDescription.setText(date);
            Glide.with(viewHolder.itemView).load(dataList.get(position)).into(viewHolder.imageViewBackground);
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        public class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
            View itemView;
            ImageView imageViewBackground;
            TextView textViewDescription;

            public SliderAdapterVH(View itemView) {
                super(itemView);
                imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
                textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
                this.itemView = itemView;
            }
        }
    }
}
